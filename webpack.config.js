var webpack = require('webpack');
var BowerWebpackPlugin = require("bower-webpack-plugin");

module.exports = {
	entry: './app/app.coffee',
	output: {
		path: './dist',
		filename: 'app.bundle.js'
	},
	resolve: {
		modulesDirectories: [
			// We are using bower for frontend libs.
			//
			// "node_modules",
			// "assets/bower_components"
		]
	},
	module: {
		loaders: [{
				test: /\.coffee$/,
				loader: 'coffee'
			},
			// {
			// 	test: /\.jade$/,
			// 	loader: 'jade'
			// }
		]
	},
	debug: false,

	// Disabale all devtools on production
	// devtool: "eval-cheap-module-source-map", // For save on Coffee script source
	devtool: 'eval-cheap-source-map',
	// devtool: 'eval', // For production - less size of app.bundle.js

	plugins: [
		new webpack.optimize.UglifyJsPlugin({
			sourceMap: false,
			compress: {
				warnings: false,
			},
			output: {
				comments: false,
			},
		}),
		new BowerWebpackPlugin({
			modulesDirectories: ["assets/bower_components"],
			manifestFiles: "bower.json",
			includes: /.*/,
			excludes: [],
			searchResolveModulesDirectories: true
		})
	]
};